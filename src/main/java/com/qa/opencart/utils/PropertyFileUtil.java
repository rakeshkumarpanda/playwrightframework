package com.qa.opencart.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileUtil {
	
	/**
	 * This method is useful to read the property file. The method will return the Properties object.
	 * As a user: use getProperty(String key) to read the desired property
	 * @param filePath
	 * @return {@link Properties}
	 */
	public Properties readProperty(String filePath) {
		FileInputStream file;
		Properties properties = null;
		try {
			file = new FileInputStream(filePath);
			properties = new Properties();
			properties.load(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties;
		
	}

}
