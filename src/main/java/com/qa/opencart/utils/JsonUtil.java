package com.qa.opencart.utils;

import java.io.File;
import java.io.IOException;

import com.jayway.jsonpath.JsonPath;

public class JsonUtil {
	
	/**
	 * This method is helpful to read the json file using the json path. 
	 * <br>
	 * The json path should be from base parent/root node to the desired child node.
	 * </br>
	 * <br>
	 * The json path format can be of below type
	 * 1. $.['parentNode'].['childNode'].[soOn]
	 * 2. $.parentNode.childNode.soOn
	 * </br>
	 * <br>
	 * This method will return an object. User need to type cast during the use to the desired type
	 * </br>
	 * @param filePath
	 * @param jsonPathNode
	 * @return Object
	 */
	public Object readJson(String filePath, String jsonPathNode) {
		Object object = null;
		File file = new File(filePath);
		try {
			object = JsonPath.parse(file).read(jsonPathNode);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return object;
	}

}
