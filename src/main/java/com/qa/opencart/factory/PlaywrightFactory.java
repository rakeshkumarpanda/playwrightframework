package com.qa.opencart.factory;

import java.nio.file.Paths;
import java.util.Properties;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;

public class PlaywrightFactory {
	
	public Playwright playwright;
	public Browser browser;
	public BrowserContext browserContext;
	public Page page;
	private ThreadLocal<Playwright> threadLocal_Playwright = new ThreadLocal<Playwright>();
	private ThreadLocal<Browser> threadLocal_Browser = new ThreadLocal<Browser>();
	private ThreadLocal<BrowserContext> threadLocal_BrowserContext = new ThreadLocal<BrowserContext>();
	private static ThreadLocal<Page> threadLocal_Page = new ThreadLocal<Page>();
	
	private Playwright getPlaywright() {
		return threadLocal_Playwright.get();
	}
	
	private Browser getBrowser() {
		return threadLocal_Browser.get();
	}
	
	private BrowserContext getBrowserContext() {
		return threadLocal_BrowserContext.get();
	}
	
	private static Page getPage() {
		return threadLocal_Page.get();
	}
	
	public Page initBrowser(Properties properties) {
		String browserName = properties.getProperty("browser");
//		playwright = Playwright.create();
		threadLocal_Playwright.set(Playwright.create());
		switch (browserName.toLowerCase()) {
		case "chromium":
//			browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(false)));
			break;
		case "chromiumheadless":
//			browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(true)));
			break;
		case "firefox":
//			browser = playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().firefox().launch(new BrowserType.LaunchOptions().setHeadless(false)));
			break;
		case "firefoxheadless":
//			browser = playwright.firefox().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().firefox().launch(new BrowserType.LaunchOptions().setHeadless(true)));
			break;
		case "safari":
//			browser = playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().webkit().launch(new BrowserType.LaunchOptions().setHeadless(false)));
			break;
		case "safariheadless":
//			browser = playwright.webkit().launch(new BrowserType.LaunchOptions().setHeadless(false));
			threadLocal_Browser.set(getPlaywright().webkit().launch(new BrowserType.LaunchOptions().setHeadless(true)));
			break;
		case "chrome":
//			browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setChannel("chrome"));
			threadLocal_Browser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setChannel("chrome")));
			break;
		case "chromeheadless":
//			browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setChannel("chrome"));
			threadLocal_Browser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(true).setChannel("chrome")));
			break;
		case "edge":
//			browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setChannel("msedge"));
			threadLocal_Browser.set(getPlaywright().chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setChannel("msedge")));
			break;
		default:
			System.out.println(browserName+" is not a valid browser type.");
			break;
		}
		
		threadLocal_BrowserContext.set(getBrowser().newContext());
		threadLocal_Page.set(getBrowserContext().newPage());
		getPage().navigate(properties.getProperty("url"));
		return getPage();
		
//		page = browser.newContext().newPage();
//		page.navigate(properties.getProperty("url"));
//		return page;
	}
	
	//Capturing screenshot
	public static String takeScreenshot() {
		String path = System.getProperty("user.dir")+"/screenshot/"+System.currentTimeMillis()+".png";
		getPage().screenshot(new Page.ScreenshotOptions()
										.setFullPage(true)
										.setPath(Paths.get(path)));
		return path;
	}

}
