package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class AccountsLogoutPage {
	
	private Page page;
	
	//Locators
	private String pageHeaderText = "div#content h1";
	private String continueBUtton = "div#content a";
	
	
	public AccountsLogoutPage(Page page) {
		this.page = page;
	}
	
	public String getPageTitle() {
		return page.title();
	}
	
	public String getPageHeader() {
		return page.textContent(pageHeaderText);
	}
	
	public HomePage continueLogout() {
		page.click(continueBUtton);
		return new HomePage(page);
	}

}
