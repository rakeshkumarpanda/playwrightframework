package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class AccountsPage {
	
	private Page page;
	
	//Locators
	private String logoutLink = "//a[@class='list-group-item'][normalize-space()='Logout']";
	
	public AccountsPage(Page page) {
		this.page = page;
	}
	
	public String getAccountsPageTitle() {
		return page.title();
	}
	
	public boolean isUserInAccountsPage() {
		return page.isVisible(logoutLink);
	}
	
	public AccountsLogoutPage logout() {
		page.click(logoutLink);
		return new AccountsLogoutPage(page);
	}

}
