package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class HomePage {
	
	//Page Reference
	private Page page;
	
	//Locators
	private String searchBox = "input[name='search']";
	private String searchButton = "div#search button";
	private String searchPageHeader = "div#content>h1";
	private String myAccountLink = "a[title='My Account']";
	private String loginLink = "a:text('Login')";
	
	//Constructor to initialize the page reference
	public HomePage(Page page) {
		this.page = page;
	}
	
	//Page actions
	public String getHomePageTitle() {
		return page.title();
	}
	
	public String getHomePageURL() {
		return page.url();
	}
	
	public String doSearch(String productName) {
		page.fill(searchBox, productName);
		page.click(searchButton);
		return page.textContent(searchPageHeader);
	}
	
	public LoginPage navigateToLoginPage() {
		page.click(myAccountLink);
		page.click(loginLink);
		return new LoginPage(page);
	}

}
