package com.qa.opencart.pages;

import com.microsoft.playwright.Page;

public class LoginPage {
	
	private Page page;
	
	//Locators
	private String emailAddressField = "#input-email";
	private String passwordField = "#input-password";
	private String loginButton = "input[value='Login']";
	private String forgotPasswordLink = "div[class='form-group'] a";
	
	public LoginPage(Page page) {
		this.page = page;
	}
	
	public String getLoginPageTitle() {
		return page.title();
	}
	
	public boolean isForgotPasswordLinkVisible() {
		return page.isVisible(forgotPasswordLink);
	}
	
	public AccountsPage loginProceedToNextPage(String username, String password) {
		page.fill(emailAddressField, username);
		page.fill(passwordField, password);
		page.click(loginButton);
		return new AccountsPage(page);
	}

}
