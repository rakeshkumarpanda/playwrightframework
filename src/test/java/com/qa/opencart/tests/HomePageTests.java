package com.qa.opencart.tests;

import org.testng.annotations.Test;
import org.testng.Assert;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;
import com.qa.opencart.base.TestBase;
import com.qa.opencart.constants.AppConstants;

public class HomePageTests extends TestBase {
	
	
	@Test(priority = 1)
	public void homePageTitleTest() {
		String actualHomePageTitle = homePage.getHomePageTitle();
		Assert.assertEquals(actualHomePageTitle, AppConstants.HOME_PAGE_TITLE);
	}
	
	@Test(priority = 2)
	public void homePageURLTest() {
		String actualHomePageURL = homePage.getHomePageURL();
		Assert.assertEquals(actualHomePageURL, properties.get("url"));
	}
	
	@Test(priority = 3)
	public void productPageHeaderTest() {
		String actualProductPageHeader = homePage.doSearch("Macbook");
		Assert.assertEquals(actualProductPageHeader, "Search - Macbook");
	}
	
	@DataProvider(name = "getProductList")
	public Iterator<String> getProductNames(){
		List<String> productNames = new ArrayList<String>();
		productNames.add("Macbook");
		productNames.add("iMac");
		productNames.add("iPhone");
		return productNames.iterator();
	}
	
	@Test(dataProvider = "getProductList", priority = 4)
	public void dp_productPageHeaderTest(String productName) {
		String actualProductPageHeader = homePage.doSearch(productName);
		Assert.assertEquals(actualProductPageHeader, "Search - "+productName);
	}
	
}
