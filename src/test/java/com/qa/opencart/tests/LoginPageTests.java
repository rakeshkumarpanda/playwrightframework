package com.qa.opencart.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qa.opencart.base.TestBase;
import com.qa.opencart.constants.AppConstants;

public class LoginPageTests extends TestBase {
	
	@Test(priority = 1)
	public void verifyForgotPasswordLinkVisible() {
		loginPage = homePage.navigateToLoginPage();
		boolean isForgotPasswordLinkVisible = loginPage.isForgotPasswordLinkVisible();
		Assert.assertTrue(isForgotPasswordLinkVisible);
	}
	
	@Test(priority = 2)
	public void verifyLoginPageTitle() {
		String loginPageTitle = loginPage.getLoginPageTitle();
		Assert.assertEquals(loginPageTitle, AppConstants.LOGIN_PAGE_TITLE);
	}
	
	@Test(priority = 3)
	public void loginTest() throws Exception {
		accountsPage = loginPage.loginProceedToNextPage(properties.getProperty("username"), properties.getProperty("password"));
		Assert.assertTrue(accountsPage.isUserInAccountsPage());
	}

}
