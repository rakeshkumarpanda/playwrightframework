package com.qa.opencart.base;

import java.util.Properties;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.microsoft.playwright.Page;
import com.qa.opencart.factory.PlaywrightFactory;
import com.qa.opencart.pages.AccountsPage;
import com.qa.opencart.pages.HomePage;
import com.qa.opencart.pages.LoginPage;
import com.qa.opencart.utils.JsonUtil;
import com.qa.opencart.utils.PropertyFileUtil;

public class TestBase {
	
	PlaywrightFactory playwrightFactory;
	Page page;
	JsonUtil jsonUtil;
	PropertyFileUtil propertiesFileUtil;
	protected Properties properties;
	
	//Page References
	protected HomePage homePage;
	protected LoginPage loginPage;
	protected AccountsPage accountsPage;
	
	String configFilePath = "./src/test/resources/config/config.properties";
	
	@Parameters("browser")
	@BeforeTest(alwaysRun = true)
	public void setup(String browser) {
		propertiesFileUtil = new PropertyFileUtil();
		properties = propertiesFileUtil.readProperty(configFilePath);
		if(browser!=null)
			properties.setProperty("browser", browser);
		playwrightFactory = new PlaywrightFactory();
		page = playwrightFactory.initBrowser(properties);
		homePage = new HomePage(page);
	}
	
	@AfterTest(alwaysRun = true)
	public void tearDown() {
		page.context().browser().close();
	}

}
